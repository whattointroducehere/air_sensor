package com.evgenbondarenko.air_sensor.data.network_check;

public interface INetworkCheck {
    boolean isOnline();
}
