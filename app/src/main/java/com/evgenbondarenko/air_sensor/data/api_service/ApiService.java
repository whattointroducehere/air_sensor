package com.evgenbondarenko.air_sensor.data.api_service;

import com.evgenbondarenko.air_sensor.data.model.api.Response;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/feed/geo:{location}/")
    Single<Response> queryApi(@Path("location") String location, @Query("token") String token);
}
