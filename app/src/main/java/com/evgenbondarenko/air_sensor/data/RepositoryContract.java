package com.evgenbondarenko.air_sensor.data;

import com.evgenbondarenko.air_sensor.data.model.api.Response;

import io.reactivex.Single;

public interface RepositoryContract {
    Single<Response> locationApi(String lat,String lng);
}
