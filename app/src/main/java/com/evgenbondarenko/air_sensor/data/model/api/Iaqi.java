package com.evgenbondarenko.air_sensor.data.model.api;


import com.google.gson.annotations.SerializedName;


public class Iaqi{

	@SerializedName("p")
	private P p;

	@SerializedName("wg")
	private Wg wg;

	@SerializedName("pm25")
	private Pm25 pm25;

	@SerializedName("t")
	private T t;

	@SerializedName("w")
	private W w;

	@SerializedName("h")
	private H h;

	@SerializedName("pm10")
	private Pm10 pm10;

	public void setP(P P){
		this.p = P;
	}

	public P getP(){
		return p;
	}

	public void setWg(Wg wg){
		this.wg = wg;
	}

	public Wg getWg(){
		return wg;
	}

	public void setPm25(Pm25 pm25){
		this.pm25 = pm25;
	}

	public Pm25 getPm25(){
		return pm25;
	}

	public void setT(T T){
		this.t = T;
	}

	public T getT(){
		return t;
	}

	public void setW(W W){
		this.w = W;
	}

	public W getW(){
		return w;
	}

	public void setH(H H){
		this.h = H;
	}

	public H getH(){
		return h;
	}

	public void setPm10(Pm10 pm10){
		this.pm10 = pm10;
	}

	public Pm10 getPm10(){
		return pm10;
	}

	@Override
 	public String toString(){
		return 
			"Iaqi{" + 
			"p = '" + p + '\'' +
			",wg = '" + wg + '\'' + 
			",pm25 = '" + pm25 + '\'' + 
			",t = '" + t + '\'' +
			",w = '" + w + '\'' +
			",h = '" + h + '\'' +
			",pm10 = '" + pm10 + '\'' + 
			"}";
		}
}