package com.evgenbondarenko.air_sensor.data.model.sensor;

import android.os.Parcel;
import android.os.Parcelable;

public class Sensor implements Parcelable {
    private String lotitude;
    private String longitude;
    private String adress;
    private String pm2;
    private String pm5;
    private String pm10;

    public Sensor() {
    }

    protected Sensor(Parcel in) {
        lotitude = in.readString();
        longitude = in.readString();
        adress = in.readString();
        pm2 = in.readString();
        pm5 = in.readString();
        pm10 = in.readString();
    }

    public static final Creator<Sensor> CREATOR = new Creator<Sensor>() {
        @Override
        public Sensor createFromParcel(Parcel in) {
            return new Sensor(in);
        }

        @Override
        public Sensor[] newArray(int size) {
            return new Sensor[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lotitude);
        dest.writeString(longitude);
        dest.writeString(adress);
        dest.writeString(pm2);
        dest.writeString(pm5);
        dest.writeString(pm10);
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "lotitude='" + lotitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", adress='" + adress + '\'' +
                ", pm2='" + pm2 + '\'' +
                ", pm5='" + pm5 + '\'' +
                ", pm10='" + pm10 + '\'' +
                '}';
    }

    public String getLotitude() {
        return lotitude;
    }

    public void setLotitude(String lotitude) {
        this.lotitude = lotitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPm2() {
        return pm2;
    }

    public void setPm2(String pm2) {
        this.pm2 = pm2;
    }

    public String getPm5() {
        return pm5;
    }

    public void setPm5(String pm5) {
        this.pm5 = pm5;
    }

    public String getPm10() {
        return pm10;
    }

    public void setPm10(String pm10) {
        this.pm10 = pm10;
    }
}
