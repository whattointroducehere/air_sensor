package com.evgenbondarenko.air_sensor.data.model.api;

import com.google.gson.annotations.SerializedName;

public class T{

	@SerializedName("v")
	private String V;

	public void setV(String V){
		this.V = V;
	}

	public String getV(){
		return V;
	}

	@Override
 	public String toString(){
		return 
			"T{" + 
			"v = '" + V + '\'' + 
			"}";
		}
}