package com.evgenbondarenko.air_sensor.data.model.api;


import com.google.gson.annotations.SerializedName;


public class Pm10{

	@SerializedName("v")
	private String v;

	public void setV(String V){
		this.v = V;
	}

	public String getV(){
		return v;
	}

	@Override
 	public String toString(){
		return 
			"Pm10{" + 
			"v = '" + v + '\'' +
			"}";
		}
}