package com.evgenbondarenko.air_sensor.data;

import com.evgenbondarenko.air_sensor.BuildConfig;
import com.evgenbondarenko.air_sensor.data.api_service.ApiService;
import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.room.LocalEntry;

import javax.inject.Inject;

import io.reactivex.Single;
import timber.log.Timber;

public class Repository implements RepositoryContract {

    @Inject ApiService api;
    @Inject LocalEntry dao;



    @Inject
    public Repository() {
    }

    @Override
    public Single<Response> locationApi(String lat, String lng) {
        return api.queryApi(lat.concat(";").concat(lng), BuildConfig.TOKEN)
                .doOnError(throwable -> Timber.e("api error -> %s",throwable.getMessage()));
    }
}
