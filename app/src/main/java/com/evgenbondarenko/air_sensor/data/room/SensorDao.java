package com.evgenbondarenko.air_sensor.data.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Entry.class},version = 1, exportSchema = false)
public abstract class SensorDao extends RoomDatabase {
    public  abstract LocalEntry getDao();
}
