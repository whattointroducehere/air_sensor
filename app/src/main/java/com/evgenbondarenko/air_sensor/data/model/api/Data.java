package com.evgenbondarenko.air_sensor.data.model.api;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class Data{

	@SerializedName("iaqi")
	private Iaqi iaqi;

	@SerializedName("debug")
	private Debug debug;

	@SerializedName("city")
	private City city;

	@SerializedName("aqi")
	private String aqi;

	@SerializedName("time")
	private Time time;

	@SerializedName("idx")
	private String idx;

	@SerializedName("attributions")
	private List<AttributionsItem> attributions;

	@SerializedName("dominentpol")
	private String dominentpol;

	public void setIaqi(Iaqi iaqi){
		this.iaqi = iaqi;
	}

	public Iaqi getIaqi(){
		return iaqi;
	}

	public void setDebug(Debug debug){
		this.debug = debug;
	}

	public Debug getDebug(){
		return debug;
	}

	public void setCity(City city){
		this.city = city;
	}

	public City getCity(){
		return city;
	}

	public void setAqi(String aqi){
		this.aqi = aqi;
	}

	public String getAqi(){
		return aqi;
	}

	public void setTime(Time time){
		this.time = time;
	}

	public Time getTime(){
		return time;
	}

	public void setIdx(String idx){
		this.idx = idx;
	}

	public String getIdx(){
		return idx;
	}

	public void setAttributions(List<AttributionsItem> attributions){
		this.attributions = attributions;
	}

	public List<AttributionsItem> getAttributions(){
		return attributions;
	}

	public void setDominentpol(String dominentpol){
		this.dominentpol = dominentpol;
	}

	public String getDominentpol(){
		return dominentpol;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"iaqi = '" + iaqi + '\'' + 
			",debug = '" + debug + '\'' + 
			",city = '" + city + '\'' + 
			",aqi = '" + aqi + '\'' + 
			",time = '" + time + '\'' + 
			",idx = '" + idx + '\'' + 
			",attributions = '" + attributions + '\'' + 
			",dominentpol = '" + dominentpol + '\'' + 
			"}";
		}
}