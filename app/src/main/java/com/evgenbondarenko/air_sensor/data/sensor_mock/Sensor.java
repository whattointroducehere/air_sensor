package com.evgenbondarenko.air_sensor.data.sensor_mock;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class Sensor {
    private double pm2_5;
    private double pm5;
    private double pm10;
    private double min = 10D;
    private double max = 100D;

    @Inject
    public Sensor() {
    }

    public Observable<Double> pm2_5(){
        return Observable.interval(2, TimeUnit.SECONDS)
                .flatMap(this::random);
    }

    public Observable<Double> pm5(){
        return Observable.interval(4, TimeUnit.SECONDS)
                .flatMap(this::random);
    }

    public Observable<Double> pm10(){
        return Observable.interval(6, TimeUnit.SECONDS)
                .flatMap(this::random);
    }

    public double getPm2_5() {
        return pm2_5;
    }

    private Observable<Double> random(long interval){
        return Observable.just(ThreadLocalRandom.current().nextDouble(min, max));
    }

    public void setPm2_5(double pm2_5) {
        this.pm2_5 = pm2_5;
    }

    public double getPm5() {
        return pm5;
    }

    public void setPm5(double pm5) {
        this.pm5 = pm5;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "pm2_5=" + pm2_5 +
                ", pm5=" + pm5 +
                ", pm10=" + pm10 +
                '}';
    }

    public double getPm10() {
        return pm10;
    }

    public void setPm10(double pm10) {
        this.pm10 = pm10;
    }
}
