package com.evgenbondarenko.air_sensor.presentation.fragments.chart;

public interface ChartContract {
    interface View{}
    interface Presenter{}
}
