package com.evgenbondarenko.air_sensor.presentation;

import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainActivity;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

@Singleton
public class Router implements RouterContract {
    private BaseActivityContract baseView;


    @Inject
    Router() {
    }


    @Override
    public void startMainActivity() {
        baseView.transactionActivity(MainActivity.class, true);
    }

    @Override
    public void startView(BaseActivityContract view) {
        this.baseView = view;
        if(baseView instanceof MainActivity){
            baseView.transactionFragment(SensorsFragment.newInstance());
        }
    }

    @Override
    public void openFragment(MenuItem menuItem) {
        Timber.d("debug Router openFragment "+menuItem);
        switch (menuItem.getItemId()){
            case R.id.action_sensors: baseView.transactionFragment(SensorsFragment.newInstance());
            break;
            case R.id.action_map: baseView.transactionFragment(MapFragment.newInstance());
            break;
            case R.id.action_chart: baseView.transactionFragment(ChartFragment.newInstance());
            break;
        }
    }
}
