package com.evgenbondarenko.air_sensor.presentation.fragments.map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evgenbondarenko.air_sensor.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import timber.log.Timber;

public class MapFragment extends DaggerFragment implements MapContract.View , OnMapReadyCallback {
    private GoogleMap gmap;

    @Inject
    public MapFragment() {
    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, null, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapFragment.this);
        return view;
    }

//    @Override
//    protected int getLayoutRes() {
//        return R.layout.fragment_map;
//    }
//
//    @Override
//    protected void initFragmentView() {
//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//    }
//
//    @Override
//    protected void attachFragment() {
//
//    }
//
//    @Override
//    protected void startFragment() {
//
//    }
//
//    @Override
//    protected void stopFragment() {
//
//    }
//
//    @Override
//    protected void destroyFragment() {
//
//    }
//
//    @Override
//    protected void pauseFragment() {
//
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Timber.d("debug MapFragment onMapReady");
        gmap = googleMap;
        gmap.setMinZoomPreference(12);
        LatLng ny = new LatLng(48.4510114, 35.0483573);
        gmap.moveCamera(CameraUpdateFactory.newLatLng(ny));
    }
}
