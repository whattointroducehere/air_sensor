package com.evgenbondarenko.air_sensor.presentation.activity.main;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.databinding.ActivityMainBinding;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivity;
import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsFragment;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import timber.log.Timber;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {
    @Inject
    MainContract.Presenter mainPresenter;
    @Inject
    SettingsFragment settingsFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void createActivity(@Nullable Bundle savedInstanceState) {
        getBinding().setEvent(mainPresenter);

        getBinding().customActionBar.menuButton.setOnClickListener(v -> mainPresenter.topMenuButtonClick());
        getBinding().bottomNavigation.setOnNavigationItemSelectedListener(menuItem -> {
            mainPresenter.bottomMenuButtonClick(menuItem);
            return true;
        });

        getBinding().bottomNavigation.setSelectedItemId(R.id.action_sensors);

        //transactionFragment(sensorsFragment);

        //CustomActionBarBinding actionBarBinding = DataBindingUtil.setContentView(this,R.layout.custom_action_bar);

        //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //getSupportActionBar().setDisplayShowCustomEnabled(true);
        //getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        //actionBarBinding.menuButton.setOnClickListener(v -> presenter.menuButtonClick());
        //getSupportActionBar().getCustomView().findViewById(R.id.menuButton).setOnClickListener(v -> presenter.menuButtonClick());
    }

    @Override
    protected void stopActivity() {

    }

    @Override
    protected void startActivity() {
        mainPresenter.starView();
    }

    @Override
    protected void pauseActivity() {

    }

    @Override
    protected void resumeActivity() {

    }

    @Override
    protected void destroyActivity() {
        mainPresenter.stopView();
    }


    @Override
    public void showSettingsDialog() {
        settingsFragment.show(getSupportFragmentManager(), "settings");
    }

    @Override
    public void checkInternet(boolean check,boolean toast) {
        if (check) {
            getBinding().customActionBar.iconInternet.setColorFilter(getResources().getColor(R.color.lightOrangeColor));
        } else {
            if (toast)
                toast(getResources().getString(R.string.not_internet));
                getBinding().customActionBar.iconInternet.setColorFilter(getResources().getColor(R.color.lightBlueGrayColor));
            }
    }

    @Override
    public void checkGPS(boolean check) {

    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity, cycleFinish, object);
    }

    @Override
    public void transactionFragment(DaggerFragment daggerFragment) {
        Timber.d("debug MainActivity transactionFragment " + daggerFragment);
        super.transactionFragmentNoBackStack(daggerFragment, R.id.content_main);
    }

    //private String convertDoubleToString(Double x){
//        return String.format(Locale.US,"%.2f", x);
//    }
}
