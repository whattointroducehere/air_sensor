package com.evgenbondarenko.air_sensor.presentation.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.Objects;

import dagger.android.support.DaggerAppCompatActivity;
import dagger.android.support.DaggerFragment;

public abstract class BaseActivity<Binding extends ViewDataBinding> extends DaggerAppCompatActivity {
    private Binding binding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        createActivity(savedInstanceState);
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void createActivity(@Nullable Bundle savedInstanceState);

    protected abstract void stopActivity();

    protected abstract void startActivity();

    protected abstract void pauseActivity();

    protected abstract void resumeActivity();

    protected abstract void destroyActivity();


    protected Binding getBinding() {
        return binding;
    }


    @Override
    protected void onDestroy() {
        destroyActivity();
        super.onDestroy();
    }

    protected void transactionFragmentNoBackStack(DaggerFragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }


    protected void transactionFragmentWithBackStack(DaggerFragment fragment,int container) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

//    protected void transactionActivity(Class<?> activity,boolean cycleFinish){
//        if (activity != null) {
//            Intent intent = new Intent(this, activity);
//            startActivity(intent);
//            if(cycleFinish) {
//                this.finish();
//            }
//        }
//    }

    protected <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T object){
        if (activity != null) {
            Intent intent = new Intent(this, activity);
            if(object != null){

            }
            startActivity(intent);
            if(cycleFinish) {
                this.finish();
            }
        }
    }

    //TODO not used
    protected  void backPresed(){
        super.onBackPressed();
        FragmentManager manager = this.getSupportFragmentManager();
        while (manager.getBackStackEntryCount() == 0) {

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startActivity();
    }

    @Override
    protected void onStop() {
        stopActivity();
        super.onStop();
    }

    @Override
    protected void onPause() {
        pauseActivity();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeActivity();
    }

    public void hideKeyboard() {
        InputMethodManager imm =
                (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).showSoftInput(this.getWindow().getDecorView(), InputMethodManager.SHOW_IMPLICIT);
    }

    @SuppressLint("ShowToast")
    public void toast(String val){
        Toast.makeText(this,val,Toast.LENGTH_LONG).show();
    }

}