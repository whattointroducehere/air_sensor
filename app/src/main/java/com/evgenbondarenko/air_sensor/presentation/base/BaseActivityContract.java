package com.evgenbondarenko.air_sensor.presentation.base;

import dagger.android.support.DaggerFragment;

public interface BaseActivityContract {
     <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T... object);
     void transactionFragment(DaggerFragment daggerFragment);
}
