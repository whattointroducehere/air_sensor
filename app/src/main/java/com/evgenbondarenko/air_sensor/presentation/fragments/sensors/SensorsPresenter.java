package com.evgenbondarenko.air_sensor.presentation.fragments.sensors;

import com.evgenbondarenko.air_sensor.domain.main.MainInteractorContract;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class SensorsPresenter implements SensorsContract.Presenter{
    private CompositeDisposable disposable;

    @Inject MainInteractorContract interactor;
    @Inject SensorsContract.View view;


    @Inject
    SensorsPresenter() {
        Timber.d("init SensorsPresenter");
        disposable = new CompositeDisposable();
    }


    private Observable<Double> pm2_5() {
        return interactor.pm2_5();
        //.map(this::convertDoubleToString);
    }

    private Observable<Double> pm5() {
        return interactor.pm5();
        //.map(this::convertDoubleToString);
    }

    private Observable<Double> pm10() {
        return interactor.pm10();
        //.map(this::convertDoubleToString);
    }

    @Override
    public void init() {
        disposable.add(pm2_5()
                .subscribe(v -> view.pm2_5(v), throwable -> Timber.e("error pm 2.5 %s", throwable.getMessage())));

        disposable.add(pm5()
                .subscribe(v -> view.pm5(v), throwable -> Timber.e("error pm 5 %s", throwable.getMessage())));

        disposable.add(pm10()
                .subscribe(v -> view.pm10(v), throwable -> Timber.e("error pm 10 %s", throwable.getMessage())));
    }

    @Override
    public void dispose() {
        disposable.dispose();
    }
}
