package com.evgenbondarenko.air_sensor.presentation.activity.splash;

import androidx.annotation.Nullable;
import android.os.Bundle;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.databinding.ActivitySplashBinding;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivity;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class SplashActivity extends BaseActivity<ActivitySplashBinding> implements SplashContract.View {

    @Inject SplashContract.Presenter presenter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    protected void createActivity(@Nullable Bundle savedInstanceState) {
        getBinding().setEvent(presenter);
        //TODO реализовать заставку с задеркой на 5 секунд, реализацию задержки делать в презенторе

    }

    @Override
    protected void stopActivity() {

    }

    @Override
    protected void startActivity() {
        presenter.starView();
    }

    @Override
    protected void pauseActivity() {
        presenter.stopView();
    }

    @Override
    protected void resumeActivity() {

    }

    @Override
    protected void destroyActivity() {

    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity,cycleFinish,object);
    }

    @Override
    public void transactionFragment(DaggerFragment daggerFragment) {

    }

//    @Override
//    public void transactionFragment(MenuItem menuItem) {
//
//    }
}
