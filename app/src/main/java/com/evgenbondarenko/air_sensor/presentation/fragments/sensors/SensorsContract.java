package com.evgenbondarenko.air_sensor.presentation.fragments.sensors;

public interface SensorsContract {
    interface View {
        void pm2_5(Double value);

        void pm5(Double value);

        void pm10(Double value);
    }

    interface Presenter{
        void init();
        void dispose();
    }
}
