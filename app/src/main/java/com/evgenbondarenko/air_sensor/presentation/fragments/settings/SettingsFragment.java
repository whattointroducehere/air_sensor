package com.evgenbondarenko.air_sensor.presentation.fragments.settings;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.evgenbondarenko.air_sensor.R;
import com.evgenbondarenko.air_sensor.databinding.FragmentSettingsBinding;

import javax.inject.Inject;

import dagger.android.support.DaggerDialogFragment;

public class SettingsFragment extends DaggerDialogFragment implements SettingsContract.View {
    private FragmentSettingsBinding binding;

    @Inject
    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container,false);
        return binding.getRoot();
    }
}
