package com.evgenbondarenko.air_sensor.presentation.activity.main;

import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.model.api.Time;
import com.evgenbondarenko.air_sensor.data.network_check.INetworkCheck;
import com.evgenbondarenko.air_sensor.domain.main.MainInteractorContract;
import com.evgenbondarenko.air_sensor.presentation.RouterContract;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class MainPresenter implements MainContract.Presenter {
    private CompositeDisposable disposable;

    @Inject
    MainContract.View view;

    @Inject
    MainInteractorContract interactor;

    @Inject
    RouterContract router;

    @Inject
    INetworkCheck networkCheck;

    @Inject
    MainPresenter() {
        disposable = new CompositeDisposable();
    }

    @Override
    public void starView() {
        router.startView(view);
        disposable.add(Observable.interval(10, TimeUnit.MILLISECONDS,Schedulers.io())
                            .flatMap(v -> Observable.just(networkCheck.isOnline()))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(v -> {
                                if (v) {
                                    view.checkInternet(v,true);
                                }
                            }, throwable -> Timber.e("chek internet error -> %s", throwable.getMessage())));
        disposable.add(interactor.locationApi("48.465608", "35.055585")
                .subscribe(this::listenerApi, throwable -> Timber.e("listenerApi error -> %s", throwable.getMessage())));
    }

    private void listenerApi(Response response) {
        Timber.e("listenerApi %s", response);
    }


    @Override
    public void topMenuButtonClick() {
        Timber.d("menu button click");
        view.showSettingsDialog();
    }

    @Override
    public void bottomMenuButtonClick(MenuItem menuItem) {
        router.openFragment(menuItem);
    }

    @Override
    public void stopView() {
        disposable.clear();
        if (view != null) view = null;
        if (interactor != null) interactor = null;
    }

    //    private String convertDoubleToString(Double x){
//        return String.format(Locale.US,"%.2f", x);
//    }
}
