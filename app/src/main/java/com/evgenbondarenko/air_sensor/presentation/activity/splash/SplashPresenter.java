package com.evgenbondarenko.air_sensor.presentation.activity.splash;

import com.evgenbondarenko.air_sensor.presentation.RouterContract;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashPresenter implements SplashContract.Presenter {

    @Inject
    SplashContract.View view;
    @Inject
    RouterContract router;

    @Inject
    public SplashPresenter() {

    }

    @Override
    public void starView() {
        router.startView(view);
        Single.timer(1, TimeUnit.SECONDS)
                .observeOn(Schedulers.computation())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<Long>() {
                    @Override
                    public void onSuccess(Long aLong) {
                        router.startMainActivity();
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.tag("SplashActivity").e(e);
                    }
                });
    }

    @Override
    public void stopView() {
        if(view != null) view = null;
    }
}
