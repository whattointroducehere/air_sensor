package com.evgenbondarenko.air_sensor.presentation.activity.splash;

import com.evgenbondarenko.air_sensor.presentation.base.BasePresenter;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;

public interface SplashContract {

    interface View extends BaseActivityContract {

    }

    interface Presenter extends BasePresenter{

    }
}
