package com.evgenbondarenko.air_sensor.presentation.activity.main;

import android.view.MenuItem;

import com.evgenbondarenko.air_sensor.presentation.base.BasePresenter;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;

public interface MainContract {

    interface View extends BaseActivityContract {
        void showSettingsDialog();
        void checkInternet(boolean check,boolean toast);
        void checkGPS(boolean check);
    }

    interface Presenter extends BasePresenter {
        void topMenuButtonClick();
        void bottomMenuButtonClick(MenuItem menuItem);
    }
}
