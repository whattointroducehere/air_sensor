package com.evgenbondarenko.air_sensor.presentation.fragments.map;

public interface MapContract {
    interface View{}
    interface Presenter{}
}
