package com.evgenbondarenko.air_sensor;

import com.evgenbondarenko.air_sensor.presentation.activity.main.MainActivity;
import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;
import javax.inject.Inject;

public class Router implements RouterContract {
    private BaseActivityContract view;

    @Inject
    public Router() {

    }

    @Override
    public  void startView(BaseActivityContract view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        if (view != null) view = null;
    }

    @Override
    public void transactionMainActivity() {
       view.transactionActivity(MainActivity.class,true);
    }

    @Override
    public void backStack() {

    }
}
