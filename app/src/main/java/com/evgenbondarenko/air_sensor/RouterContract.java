package com.evgenbondarenko.air_sensor;

import com.evgenbondarenko.air_sensor.presentation.base.BaseActivityContract;

public interface RouterContract {

    void startView(BaseActivityContract view);

    void stopView();

    void transactionMainActivity();

    void backStack();




}
