package com.evgenbondarenko.air_sensor.domain.main;

import com.evgenbondarenko.air_sensor.data.RepositoryContract;
import com.evgenbondarenko.air_sensor.data.SharedPreferencesStorage;
import com.evgenbondarenko.air_sensor.data.model.api.Response;
import com.evgenbondarenko.air_sensor.data.sensor_mock.Sensor;
import com.evgenbondarenko.air_sensor.domain.BaseInteractor;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

public class MainInteractor extends BaseInteractor implements MainInteractorContract {

    @Inject Sensor sensor;
    @Inject RepositoryContract repository;
    @Inject SharedPreferencesStorage pref;

    @Inject
    public MainInteractor() {

    }

    @Override
    public Single<Response> locationApi(String lat, String lng) {
        return repository.locationApi(lat,lng)
                .compose(applySingleSchedulers());
    }

    @Override
    public Observable<Double> pm2_5() {
        return sensor.pm2_5()
                .doOnError(throwable -> Timber.tag("Sensor").e(" pm 2.5 %s",throwable.getMessage()))
                .compose(applyObservableSchedulers());
    }

    @Override
    public Observable<Double> pm5() {
        return sensor.pm5()
                .doOnError(throwable -> Timber.tag("Sensor").e(" pm 5 %s",throwable.getMessage()))
                .compose(applyObservableSchedulers());
    }

    @Override
    public Observable<Double> pm10() {
        return sensor.pm10()
                .doOnError(throwable -> Timber.tag("Sensor").e(" pm 10 %s",throwable.getMessage()))
                .compose(applyObservableSchedulers());
    }
}
