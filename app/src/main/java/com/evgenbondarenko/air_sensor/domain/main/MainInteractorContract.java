package com.evgenbondarenko.air_sensor.domain.main;

import com.evgenbondarenko.air_sensor.data.model.api.Response;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface MainInteractorContract {
    Single<Response> locationApi(String lat, String lng);

    Observable<Double> pm2_5();

    Observable<Double> pm5();

    Observable<Double> pm10();

}
