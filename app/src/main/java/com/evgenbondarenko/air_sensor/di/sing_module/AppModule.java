package com.evgenbondarenko.air_sensor.di.sing_module;

import androidx.room.Room;
import android.content.Context;
import com.evgenbondarenko.air_sensor.App;
import com.evgenbondarenko.air_sensor.ConstantsApp;
import com.evgenbondarenko.air_sensor.data.Repository;
import com.evgenbondarenko.air_sensor.data.RepositoryContract;
import com.evgenbondarenko.air_sensor.data.SharedPreferencesStorage;
import com.evgenbondarenko.air_sensor.data.network_check.INetworkCheck;
import com.evgenbondarenko.air_sensor.data.network_check.NetworkCheck;
import com.evgenbondarenko.air_sensor.data.room.LocalEntry;
import com.evgenbondarenko.air_sensor.data.room.SensorDao;
import com.evgenbondarenko.air_sensor.presentation.Router;
import com.evgenbondarenko.air_sensor.presentation.RouterContract;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class AppModule {
    @Singleton
    @Provides
    static Context provideContext(App app){return app;}

    @Singleton
    @Provides
    static SharedPreferencesStorage provideSharedPreferencesStorage(Context context){
        return new SharedPreferencesStorage(context);
    }

    @Singleton
    @Provides
    static SensorDao provideRoomDataBase(Context context) {
        return Room.databaseBuilder(
                context.getApplicationContext(), SensorDao.class, ConstantsApp.DAO_NAME)
                .allowMainThreadQueries()
                .build();
    }

    @Singleton
    @Provides
    static RepositoryContract provideRepository(Repository repository){
        return repository;
    }

    @Singleton
    @Provides
    static LocalEntry provideLocaleDao(SensorDao dao) {
        return dao.getDao();
    }

    @Singleton
    @Provides
    static INetworkCheck provideNetworkCheck(App app){
        return new NetworkCheck(app);
    }

    @Singleton
    @Provides
    static RouterContract provideRouter(Router router){
        return router;
    }
}
