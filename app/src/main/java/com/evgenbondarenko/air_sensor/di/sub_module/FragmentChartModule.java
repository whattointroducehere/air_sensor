package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.chart.ChartPresenter;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class FragmentChartModule {
    @Binds
    abstract ChartContract.Presenter bindFragmentChartPresenter (ChartPresenter presenter);

    @Binds
    abstract ChartContract.View bindFragmentChartView (ChartFragment chartFragment);
}
