package com.evgenbondarenko.air_sensor.di.sing_module;

import com.evgenbondarenko.air_sensor.di.sub_module.BuildersModuleFragment;
import com.evgenbondarenko.air_sensor.di.sub_module.MainModule;
import com.evgenbondarenko.air_sensor.di.sub_module.SplashModule;
import com.evgenbondarenko.air_sensor.presentation.activity.main.MainActivity;
import com.evgenbondarenko.air_sensor.presentation.activity.splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModuleActivity {

    @ContributesAndroidInjector(modules = {SplashModule.class})
    abstract SplashActivity provideSplashActivity();


    @ContributesAndroidInjector(modules = {BuildersModuleFragment.class, MainModule.class})
    abstract MainActivity provideMainActivity();

}
