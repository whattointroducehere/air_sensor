package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.map.MapPresenter;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class FragmentMapModule {
    @Binds
    abstract MapContract.Presenter bindFragmentMapPresenter (MapPresenter presenter);

    @Binds
    abstract MapContract.View bindFragmentMapView (MapFragment mapFragment);
}
