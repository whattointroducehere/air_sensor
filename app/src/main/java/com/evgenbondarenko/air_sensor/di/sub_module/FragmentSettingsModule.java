package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.settings.SettingsPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class FragmentSettingsModule {
    @Binds
    abstract SettingsContract.Presenter bindFragmentPresenter (SettingsPresenter presenter);

    @Binds
    abstract SettingsContract.View bindFragmentView (SettingsFragment settingsFragment);
}
