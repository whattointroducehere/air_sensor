package com.evgenbondarenko.air_sensor.di;

import com.evgenbondarenko.air_sensor.App;
import com.evgenbondarenko.air_sensor.di.sing_module.ApiModule;
import com.evgenbondarenko.air_sensor.di.sing_module.AppModule;
import com.evgenbondarenko.air_sensor.di.sing_module.BuildersModuleActivity;

import javax.inject.Singleton;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApiModule.class,
        AppModule.class,
        BuildersModuleActivity.class

})
public interface AppComponent extends AndroidInjector<App> {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App>{}
}

