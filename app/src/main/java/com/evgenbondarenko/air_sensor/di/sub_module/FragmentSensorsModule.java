package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsContract;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsFragment;
import com.evgenbondarenko.air_sensor.presentation.fragments.sensors.SensorsPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class FragmentSensorsModule {
    @Binds
    abstract SensorsContract.Presenter bindFragmentSensorsPresenter (SensorsPresenter presenter);

    @Binds
    abstract SensorsContract.View bindFragmentSensorsView (SensorsFragment sensorsFragment);
}
