package com.evgenbondarenko.air_sensor.di.sub_module;

import com.evgenbondarenko.air_sensor.presentation.activity.splash.SplashActivity;
import com.evgenbondarenko.air_sensor.presentation.activity.splash.SplashContract;
import com.evgenbondarenko.air_sensor.presentation.activity.splash.SplashPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SplashModule {

    @Binds
    abstract SplashContract.Presenter bindSplashPresenter(SplashPresenter presenter);

    @Binds
    abstract SplashContract.View bindSplashView(SplashActivity activity);

}
